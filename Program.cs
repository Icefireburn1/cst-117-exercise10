﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CSharpEndsInEorT
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> list = new List<string>();

            Console.WriteLine("Getting file from " + AppDomain.CurrentDomain.BaseDirectory + "words.txt");
            string text = System.IO.File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "words.txt");
            list = text.Split(' ').ToList();
            for (int i = 0; i < list.Count; i++)
            {
                list[i] = Regex.Replace(list[i], "[^0-9a-zA-Z]+", "");
            }
                
            int counter = 0;
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Length - 1 > 0)
                    if (list[i][list[i].Length - 1] == 't' || list[i][list[i].Length - 1] == 'e')
                        counter++;
            }
            Console.WriteLine("There are {0} words that end in t or e", counter);
        }
    }
}
